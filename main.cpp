#include <iostream>
#include <stdlib.h>
using namespace std;

int main() {
    string fname;
    string lname;
    string ord;
    float hm;
    float quan;
    // TODO: Make a Program to ask a person about their order. 
    // It should have a Price, and quantity.

    cout << "Enter your first name: ";
    cin >> fname;
    cout << "Enter your last name: ";
    cin >> lname;
    cout << "Enter your order: ";
    cin >> ord;
    if (ord == "chicken" || ord == "Chicken") {
      cout << "The chicken costs $150.\n";
      exit(0);
    }
    return 0;
}
